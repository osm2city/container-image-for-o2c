FROM ubuntu:24.04 as build


# Set timezone:
RUN ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone

# Install basic tools and dependencies
RUN apt-get -y update &&\
        apt-get install -y git sudo dctrl-tools autoconf automake autotools-dev cmake cpp g++ gcc sed \
        libboost-dev libboost-program-options-dev libboost-system-dev libboost-chrono-dev libboost-atomic-dev libboost-date-time-dev \
        libboost-serialization-dev libboost-filesystem-dev libboost-system-dev libboost-thread-dev libplib-dev \
        libfreetype-dev libjpeg-dev libgif-dev libtiff-dev libpng-dev libxmu-dev libxi-dev zlib1g libcgal-dev libopenal-dev \
        libcurl4-openssl-dev libgdal-dev

RUN useradd --create-home --home-dir=/home/flightgear --shell=/bin/false flightgear
USER flightgear

ARG BUILD_ROOT_DIR=/home/flightgear/build/

# Use an older version of FGMeta, so we do not get an abort due to
# "Running download_and_compile.sh as root is not supported; aborting."
ENV FGMETA_VERSION 45917d6075d942437dc8b63b7ec9d84ffe60b408
# versions are as per primo September 2024
ENV SG_VERSION 65b1bf3a6222498b61df2c0e5a1b80b08dde410f
ENV FG_VERSION 3a869de51b505a4a7805547530d9f72d178e561c
# version fetched by DNC script for next = OpenSceneGraph-3.6 (Dec 1 2022)
ENV OSG_VERSION 925270d9094d87f925003cd173037601ac637caa

WORKDIR $BUILD_ROOT_DIR
RUN git clone https://git.code.sf.net/p/flightgear/fgmeta
WORKDIR $BUILD_ROOT_DIR/fgmeta
RUN git checkout $FGMETA_VERSION

WORKDIR $BUILD_ROOT_DIR
RUN mkdir dnc-managed

WORKDIR $BUILD_ROOT_DIR/dnc-managed
RUN git clone https://git.code.sf.net/p/flightgear/simgear
RUN git clone https://git.code.sf.net/p/flightgear/flightgear
# using a specific folder name to be consistent with the DNC-script (otherwise folder is OpenSceneGraph)
RUN git clone https://github.com/openscenegraph/OpenSceneGraph.git openscenegraph

WORKDIR $BUILD_ROOT_DIR/dnc-managed/simgear
RUN git checkout $SG_VERSION

WORKDIR $BUILD_ROOT_DIR/dnc-managed/flightgear
RUN git checkout $FG_VERSION

WORKDIR $BUILD_ROOT_DIR/dnc-managed/openscenegraph
RUN git checkout $OSG_VERSION

# option -dn tells the script not to download
# option -pn tells not to use a package manager to install
# DATA is not built because we will use a fake one with limited content
WORKDIR $BUILD_ROOT_DIR/dnc-managed
RUN export SG_CMAKEARGS="-DENABLE_TESTS=OFF"
RUN ../fgmeta/download_and_compile.sh -j $(nproc) -dn -pn SIMGEAR FGFS OSG

# finally we need easy-osm2city-podman due to its minimal FGDATA - we will just use the newest version
WORKDIR $BUILD_ROOT_DIR
RUN git clone https://git.nayala.duckdns.org/fly/easy-osm2city-podman.git


# ================= Final image =====================
FROM ubuntu:24.04

LABEL maintainer="rick@vanosten.net"
LABEL org.opencontainers.image.authors="rick@vanosten.net"
LABEL org.opencontainers.image.title="osm2city tooling"
LABEL org.opencontainers.image.description="osm2city tooling to generate scenery for FlightGear based on OSM data."
LABEL org.opencontainers.image.documentation="https://gitlab.com/osm2city/container-image-for-o2c/-/blob/main/README.md"
LABEL org.opencontainers.image.source="https://gitlab.com/osm2city/container-image-for-o2c"
LABEL org.opencontainers.image.vendor="Private - see maintainer"
LABEL org.opencontainers.image.licenses="GPL-2.0"
LABEL org.opencontainers.image.base.digest="ubuntu:24.04"

# RUN groupadd --gid 1001 flightgear
# RUN useradd --uid 1001 --gid flightgear --create-home --home-dir=/home/flightgear --shell=/bin/bash flightgear

# ----------------- Pre-requisites for osm2gear ------

RUN apt-get -y update

# minimal stuff for vcpkg - what is mentioned in https://learn.microsoft.com/en-us/vcpkg/get_started/get-started?pivots=shell-bash
# is not enough
RUN apt-get install -y git g++ curl zip cmake bzip2 libbz2-1.0 libbz2-dev

ARG BASE_DIR=/home/flightgear

WORKDIR $BASE_DIR
RUN git clone https://github.com/Microsoft/vcpkg.git
WORKDIR $BASE_DIR/vcpkg
RUN ./bootstrap-vcpkg.sh

ENV VCPKG_ROOT=$BASE_DIR/vcpkg
ENV PATH=$VCPKG_ROOT:$PATH

# FlightGear stuff built
WORKDIR $BASE_DIR
RUN mkdir install
ENV FG_INSTALL=/home/flightgear/install

WORKDIR $FG_INSTALL
COPY --from=build /home/flightgear/build/dnc-managed/install/simgear $FG_INSTALL/simgear
COPY --from=build /home/flightgear/build/dnc-managed/install/flightgear $FG_INSTALL/flightgear
COPY --from=build /home/flightgear/build/dnc-managed/install/openscenegraph $FG_INSTALL/openscenegraph
COPY --from=build /home/flightgear/build/easy-osm2city-podman/full/fgdata $FG_INSTALL/flightgear/fgdata


# get the dependencies for osm2gear, which are fetched from Ubuntu and not using vcpkg
RUN apt-get -y update
RUN apt-get install -y postgresql-server-dev-all libpq-dev libpqxx-7.8t64 libpqxx-dev libopengl-dev libgl1-mesa-dev freeglut3-dev protobuf-compiler libboost-all-dev proj-bin libproj-dev

# ----------------- Compile osm2gear -----------------

ENV OSM2GEAR_COMMIT ccd9355ba5a1a2eadcfbc40b63b320bff8e1e04c

WORKDIR $BASE_DIR
RUN git clone https://gitlab.com/osm2city/osm2gear.git
WORKDIR $BASE_DIR/osm2gear
RUN git checkout $OSM2GEAR_COMMIT

# get the compiler we are using
RUN apt-get -y update
RUN apt-get install -y clang-17

RUN protoc --cpp_out=$BASE_DIR/osm2gear/src/proto buildings.proto

RUN mkdir build
# RUN cmake --preset=default
RUN cmake -D CMAKE_BUILD_TYPE="Release" -D CMAKE_CXX_COMPILER=/usr/bin/clang++-17 -D CMAKE_TOOLCHAIN_FILE=$VCPKG_ROOT/scripts/buildsystems/vcpkg.cmake
RUN make -j $(nproc)

# ----------------- Prepare osm2city -----------------

ENV OSM2CITY_COMMIT a55bf0bb99c01c6c97ac290ecee802fba82991b0
ENV OSM2CITY_DATA_COMMIT 51e7c2a1b6ed7a40a3557820e3e2e807b0b65ded

WORKDIR $BASE_DIR
RUN git clone https://gitlab.com/osm2city/osm2city.git
WORKDIR $BASE_DIR/osm2city
RUN git checkout ws3
RUN git checkout $OSM2CITY_COMMIT

RUN protoc --proto_path=$BASE_DIR/osm2gear --python_out=osm2city/proto $BASE_DIR/osm2gear/buildings.proto


WORKDIR $BASE_DIR
RUN git clone https://gitlab.com/osm2city/osm2city-data.git
WORKDIR $BASE_DIR/osm2city-data
RUN git checkout $OSM2CITY_DATA_COMMIT

# ----------------- Prepare Python -------------------

RUN apt-get -y update
RUN apt-get install -y python3.12 python3.12-venv

# now use a local user in order not to be root
# USER flightgear

WORKDIR $BASE_DIR
RUN python3 -m venv venv/
RUN /home/flightgear/venv/bin/python3 -m pip install -r /home/flightgear/osm2city/requirements.txt

# ----------------- Set the default ENV variables ----
ENV O2C_PATH_TO_FG_ELEV=$FG_INSTALL/flightgear/bin/fgelev
ENV O2C_DB_HOST="127.0.0.1"
ENV O2C_DB_PORT=5432
ENV O2C_DB_USER="gisuser"
ENV O2C_DB_PASSWORD="**"
ENV O2C_PATH_TO_O2G=/home/flightgear/osm2gear/src/MainCLI
ENV O2C_PATH_TO_DATA=/home/flightgear/osm2city-data
ENV FG_ROOT=$FG_INSTALL/flightgear/fgdata
ENV LD_LIBRARY_PATH=$FG_INSTALL/simgear/lib:$FG_INSTALL/openscenegraph/lib
ENV PYTHONPATH=/home/flightgear/osm2city

# now the working directoy which will be mounted
WORKDIR $BASE_DIR
RUN mkdir working_dir
WORKDIR $BASE_DIR/working_dir


CMD ["/bin/bash"]
