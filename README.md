# Container image for o2c

This repo contains scripts and configuration to create a container for the basic `osm2city` toolchain targeting the NEXT version of [FlightGear](https://www.flightgear.org/), which amongst others includes [World Scenery 3.0](https://wiki.flightgear.org/World_Scenery_3.0_roadmap).

For the current stable version (2020.3.x) of FlightGear there is a container with extensive scripting [available](https://git.merspieler.tk/fly/easy-osm2city-podman/src/branch/master).

The main goal of providing a container is to make it much easier for users and to reduce the amount of documentation needed for installation (because a container image definition pretty much does that).

# Project status

The container image is usable, but has only been tested by the author on a recent 64-bit Linux distro using [Docker](https://www.docker.com).

# Usage

The commands below assume that you are using `Docker`. Other tools like [Podman](https://podman.io/) might have similar commands and syntax.

## Getting the container

The easiest way to get the container is to download the latest published image using `docker pull vanosten/o2c_container:latest`.

Alternatively, you can build your own image. You need to be in the directory, where this file and the Dockerfile are situated.

```
$ docker build -t o2c_container:latest .
```

Depending on you machine the build might take 15-30 minutes or longer. You can use flag `--no-cache` to build from scratch.

To upload to docker hub:

```
$ docker login
$ docker tag o2c_container vanosten/o2c_container:latest
$ docker push vanosten/o2c_container:latest
```

## Running the container

Before running the container, you have to prepare a few things:

* Install `PostgreSQL` and `PostGIS`. On Ubuntu 24.04: `postgresql-16`, `postgis`, `postgresql-16-postgis-3`, `postgresql-client-16`, `postgresql-common`
* Install [osmosis](https://github.com/openstreetmap/osmosis/releases) and a related JDK (0.48.3 of 'osmosis' uses Java 17 - v0.49.2 has issues with "out of space" https://github.com/openstreetmap/osmosis/issues/144).
* Know the user, password, host and port of your database server. If you use `gisuser` as database user and port `5432`, then you only need to specify the password and host as below. Even if you run your database server on the same machine as the container, then you still need to set the host - on linux issue `ifconfig` to get the IP-address of your computer.
* You need to prepare the `params.py` file and place it into the directory on your host, which will be mounted as `/home/flightgear/working_dir`. The paths in the `params.py` must be relative to the file system in the container (e.g. `PATH_TO_SCENERY = '/home/flightgear/working_dir/scenery'`, `OVERLAP_CHECK_APT_DAT_SCENERY_LIST = ['home/flightgear/working_dir/airports']`) - which is easiest if they are all in the same directory structure on the host (in the example below `/home/vanosten/custom_scenery`).
* Parameter `USE_WS30_SCENERY` should always be set to `True`, because no testing is done with [WS2](https://wiki.flightgear.org/FlightGear_World_Scenery_2.0). It might work, but the content generated uses some features, which are not available in the current stable version for FlightGear (2020.3.x) - the users will have to use a FlightGear NEXT version (development).

```
$ docker run --rm --env O2C_DB_PASSWORD=*** --env O2C_DB_HOST=192.168.87.118 --mount "type=bind,source=/home/vanosten/custom_scenery,target=/home/flightgear/working_dir" --user $(id -u) -it o2c_container:latest
```


## Using the running container

Once the container runs, then you get a [bash shell](https://www.gnu.org/software/bash/). First you need to activate the Python environment. Then you issue the command to generate osm2city.

Note that unless you have done something specific parametrization of the container environment, then the container can use all resources on the system and eventually bring the host down. Therefore, be mindful about how much you can increase the value of `-p` (the number of parallel processes) and make some experiments in densely populated areas before committing to a value. The main concerns are:

* RAM: `fgelev` (the process probing terrain for elevation data) can take up to 3 GB of RAM per process - and in some edge cases up to 3 `fgelev` per parallel process can be active. The RAM for `osm2city`/`osm2gear` is a minor concern. If your database server also runs on the same host, then it also will be hungry for RAM.
* Disk performance: `osm2city` writes several GB of data for an area like Switzerland. And if the database server runs on the same host, then disk I/O can be a real concern. Using a [SSD](https://en.wikipedia.org/wiki/Solid-state_drive) can make a huge difference.
* Disk space: `osm2city` uses space in the range of few to many GBs. If you look at the download size of OSM data (e.g. for Europe on [Geofabrik](http://download.geofabrik.de/europe.html)), then Switzerland is around 0.5 GB. However, the database import will be maybe a factor 10 larger and the `osm2city` output a factor 5. logfiles etc. can also contribute to space usage - but especially enabling debug plotting uses a lot of space (and debug plotting is really only for debugging a few tiles at a time).
* CPU: 1 core per process is normally fine.

```
# source /home/flightgear/venv/bin/activate
# python /home/flightgear/osm2city/build_tiles.py -f params.py -b *7_46.5_7.25_46.625 -o -p 1 -e all

...

#
```

# Credits
The container image draws inspiration from [easy-osm2city-podman](https://git.merspieler.tk/fly/easy-osm2city-podman/src/branch/master) as well as [FlightGear container for WS3.0](https://sourceforge.net/p/flightgear/fgmeta/ci/next/tree/ws30/ws30-vpb-generator-docker/).
